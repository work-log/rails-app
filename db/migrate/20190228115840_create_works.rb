class CreateWorks < ActiveRecord::Migration[5.2]
  def change
    create_table :works do |t|
      t.date :date
      t.text :description

      t.timestamps
    end
  end
end
